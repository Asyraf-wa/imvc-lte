<?php
/**
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link http://cakephp.org CakePHP(tm) Project
 * @package Cake.Console.Templates.default.views
 * @since CakePHP(tm) v 1.2.0.5234
 * @license MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
?>
<div class="header_title"><b class="tajuk"><i class="fas fa-bars"></i> <?php printf("<?php echo __('%s'); ?>", Inflector::humanize($singularHumanName), $singularHumanName); ?></b> <?php printf("<?php echo __('%s'); ?>", Inflector::humanize($action), $singularHumanName); ?></div>
<hr>

<div class="row">
	<div class="col-md-4">
<div class="box box-primary">
	<div class="box-header with-border">
		<h3 class="box-title"><i class="fas fa-bars"></i> Title</h3>
		<div class="box-tools pull-right">
			<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
		</div>
	</div>
	<div class="box-body">
<div class="justify">
Content
</div>
	</div>
</div>

<div class="box box-primary">
	<div class="box-header with-border">
		<h3 class="box-title"><i class="fa fa-bars" aria-hidden="true"></i> Related Menu</h3>
		<div class="box-tools pull-right">
			<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
		</div>
	</div>
	<div class="box-body">
							<?php if (strpos($action, 'add') === false): ?>
									<?php echo "<?php echo \$this->Form->postLink(__('<i class=\"fa fa-trash-o\" aria-hidden=\"true\"></i>&nbsp;&nbsp;Delete'), array('action' => 'delete', \$this->Form->value('{$modelClass}.{$primaryKey}')), array('class'=>'btn btn-danger btn-block', 'escape' => false), __('Are you sure you want to delete # %s?', \$this->Form->value('{$modelClass}.{$primaryKey}'))); ?>"; ?>
							<?php endif; ?>
									<?php echo "<?php echo \$this->Html->link(__('<span class=\"glyphicon glyphicon-list\"></span>&nbsp;&nbsp;List " . $pluralHumanName . "'), array('action' => 'index'), array('class'=>'btn btn-warning btn-block', 'escape' => false)); ?>"; ?>
							<?php
									$done = array();
									foreach ($associations as $type => $data) {
										foreach ($data as $alias => $details) {
											if ($details['controller'] != $this->name && !in_array($details['controller'], $done)) {
												echo "\t\t<?php echo \$this->Html->link(__('<i class=\"fa fa-list\" aria-hidden=\"true\"></i>&nbsp;&nbsp;List " . Inflector::humanize($details['controller']) . "'), array('controller' => '{$details['controller']}', 'action' => 'index'), array('class'=>'btn btn-warning btn-block', 'escape' => false)); ?> \n";
												echo "\t\t<?php echo \$this->Html->link(__('<i class=\"fa fa-plus\" aria-hidden=\"true\"></i>&nbsp;&nbsp;New " . Inflector::humanize(Inflector::underscore($alias)) . "'), array('controller' => '{$details['controller']}', 'action' => 'add'), array('class'=>'btn btn-primary btn-block', 'escape' => false)); ?> \n";
												$done[] = $details['controller'];
											}
										}
									}
							?>
	</div>
</div>

	</div>
  <div class="col-md-8">
		<div class="box box-primary">
	<div class="box-header with-border">
		<h3 class="box-title"><i class="fas fa-bars"></i> <?php printf("<?php echo __('%s %s'); ?>", Inflector::humanize($action), $singularHumanName); ?></h3>
		<div class="box-tools pull-right">
			<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
		</div>
	</div>
			<div class="box-body table-responsive">
		
			<?php echo "<?php echo \$this->Form->create('{$modelClass}', array('role' => 'form')); ?>\n"; ?>

				<fieldset>

					<?php
						foreach ($fields as $field) {
							if (strpos($action, 'add') !== false && $field == $primaryKey) {
								continue;
							} elseif (!in_array($field, array('created', 'modified', 'updated'))) {
								echo "\t\t\t\t\t<div class=\"form-group\">\n";
								echo "\t\t\t\t\t\t<?php echo \$this->Form->input('{$field}', array('class' => 'form-control','label' => '{$field}')); ?>\n";
								echo "\t\t\t\t\t</div><!-- .form-group -->\n";
							}
						}
						if (!empty($associations['hasAndBelongsToMany'])) {
							foreach ($associations['hasAndBelongsToMany'] as $assocName => $assocData) {
								echo "\t\t\t\t\t<div class=\"form-group\">\n";
								echo "\t\t\t\t\t\t\t<?php echo \$this->Form->input('{$assocName}');?>\n";
								echo "\t\t\t\t\t</div><!-- .form-group -->\n";
							}
						}
						echo "\n";
					
					?>
					
<div class="box-footer">				
	<div class="pull-right">
		<div class="btn-group" role="group" aria-label="...">					
<div class="btn-group" role="group">
<?php echo "\t\t\t\t\t<?php echo \$this->Form->submit('Submit', array('class' => 'btn btn-primary btn-flat')); ?>\n"; ?>

</div>
<div class="btn-group" role="group">
<?php echo "<?php echo \$this->Html->link('Reset', array('action' => 'add'),array('class'=>'btn btn-warning btn-flat',  'escape' => false)); ?>\n"; ?>
</div>
<div class="btn-group" role="group">
<?php echo "<?php echo \$this->Html->link('Cancel', array('action' => 'index'),array('class'=>'btn btn-danger btn-flat',  'escape' => false)); ?>\n"; ?>
</div>
		</div>
	</div>
</div>	

				</fieldset>

			<?php echo "\t\t\t<?php echo \$this->Form->end(); ?>\n";?>

		</div>
			
	</div>
  
  </div>

</div>
