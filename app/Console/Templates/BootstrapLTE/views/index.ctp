<?php
/**
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       Cake.Console.Templates.default.views
 * @since         CakePHP(tm) v 1.2.0.5234
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
?>
<?php echo "<?php echo \$this->Html->css('datatables/dataTables.bootstrap'); ?>\n"; ?>
<div class="header_title"><b class="tajuk"><i class="fas fa-bars"></i> <?php printf("<?php echo __('%s'); ?>", Inflector::humanize($singularHumanName), $singularHumanName); ?></b> <?php printf("<?php echo __('%s'); ?>", Inflector::humanize($action), $singularHumanName); ?></div>
<hr>
<div class="row">
	<div class="col-xs-4">
<div class="box box-primary">
	<div class="box-header with-border">
		<h3 class="box-title"><i class="fa fa-bars" aria-hidden="true"></i> Related Menu</h3>
		<div class="box-tools pull-right">
			<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
		</div>
	</div>
	<div class="box-body">
								<?php echo "<?php echo \$this->Html->link(__('<i class=\"fa fa-plus\" aria-hidden=\"true\"></i>&nbsp;&nbsp;New " . $singularHumanName . "'), array('action' => 'add'), array('class'=>'btn btn-primary btn-block', 'escape' => false)); ?>"; ?>
						<?php
							$done = array();
							foreach ($associations as $type => $data) {
								foreach ($data as $alias => $details) {
									if ($details['controller'] != $this->name && !in_array($details['controller'], $done)) {
										echo "\t\t<?php echo \$this->Html->link(__('<i class=\"fa fa-list\" aria-hidden=\"true\"></i>&nbsp;&nbsp;List " . Inflector::humanize($details['controller']) . "'), array('controller' => '{$details['controller']}', 'action' => 'index'), array('class'=>'btn btn-warning btn-block btn-flat', 'escape' => false)); ?> \n";
										echo "\t\t<?php echo \$this->Html->link(__('<i class=\"fa fa-plus\" aria-hidden=\"true\"></i>&nbsp;&nbsp;New " . Inflector::humanize(Inflector::underscore($alias)) . "'), array('controller' => '{$details['controller']}', 'action' => 'add'), array('class'=>'btn btn-primary btn-block btn-flat', 'escape' => false)); ?> \n";
										$done[] = $details['controller'];
									}
								}
							}
						?>
	</div>
</div>
	</div>


    <div class="col-xs-8">

    <div class="box box-primary">
		<div class="box-header">
			<h3 class="box-title"><i class="fas fa-bars"></i> <?php echo "<?php echo __('{$pluralHumanName}'); ?>"; ?></h3>
			<div class="box-tools pull-right">
                <?php echo "<?php echo \$this->Html->link(__('<i class=\"glyphicon glyphicon-plus\"></i> New ".$singularHumanName."'), array('action' => 'add'), array('class' => 'btn btn-primary btn-flat btn-xs', 'escape' => false)); ?>\n"; ?>
            </div>
		</div>	
			<div class="box-body table-responsive">
                <table id="<?php echo str_replace(' ', '', $pluralHumanName); ?>" class="table table-bordered table-striped">
					<thead>
						<tr>
						<?php foreach ($fields as $field): ?>
							<th class="text-center"><?php echo "<?php echo \$this->Paginator->sort('{$field}'); ?>"; ?></th>
						<?php endforeach; ?>
						<th class="text-center"><?php echo "<?php echo __('Actions'); ?>"; ?></th>
						</tr>
					</thead>
					<tbody>
					<?php 
						echo "<?php foreach (\${$pluralVar} as \${$singularVar}): ?>\n";
						echo "\t<tr>\n";
						foreach ($fields as $field) {
							$isKey = false;
							if (!empty($associations['belongsTo'])) {
								foreach ($associations['belongsTo'] as $alias => $details) {
									if ($field === $details['foreignKey']) {
										$isKey = true;
										echo "\t\t<td class=\"text-center\">\n\t\t\t<?php echo \$this->Html->link(\${$singularVar}['{$alias}']['{$details['displayField']}'], array('controller' => '{$details['controller']}', 'action' => 'view', \${$singularVar}['{$alias}']['{$details['primaryKey']}'])); ?>\n\t\t</td>\n";
										break;
									}
								}
							}
							if ($isKey !== true) {
								echo "\t\t<td class=\"text-center\"><?php echo h(\${$singularVar}['{$modelClass}']['{$field}']); ?>&nbsp;</td>\n";
							}
						}

						echo "\t\t<td class=\"text-center\">\n";
						echo "\t\t\t<?php echo \$this->Html->link(__('<i class=\"glyphicon glyphicon-eye-open\"></i>'), array('action' => 'view', \${$singularVar}['{$modelClass}']['{$primaryKey}']), array('class' => 'btn btn-flat btn-primary btn-xs', 'escape' => false, 'data-toggle'=>'tooltip', 'title' => 'view')); ?>\n";
						echo "\t\t\t<?php echo \$this->Html->link(__('<i class=\"glyphicon glyphicon-pencil\"></i>'), array('action' => 'edit', \${$singularVar}['{$modelClass}']['{$primaryKey}']), array('class' => 'btn btn-flat btn-warning btn-xs', 'escape' => false, 'data-toggle'=>'tooltip', 'title' => 'edit')); ?>\n";
						echo "\t\t\t<?php echo \$this->Form->postLink(__('<i class=\"glyphicon glyphicon-trash\"></i>'), array('action' => 'delete', \${$singularVar}['{$modelClass}']['{$primaryKey}']), array('class' => 'btn btn-danger btn-flat btn-xs', 'escape' => false, 'data-toggle'=>'tooltip', 'title' => 'delete'), __('Are you sure you want to delete # %s?', \${$singularVar}['{$modelClass}']['{$primaryKey}'])); ?>\n";
						echo "\t\t</td>\n";
						echo "\t</tr>\n";

						echo "<?php endforeach; ?>\n";
					?>
					</tbody>
				</table>
				<p>
				<small><?php echo "<?php echo \$this->Paginator->counter(array('format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')));?>"; ?></small>
			</p>

			<?php
				echo "<?php\n";
				echo "\t\t\t\$params = \$this->Paginator->params();\n";
				echo "\t\t\tif (\$params['pageCount'] > 1) {\n";
				echo "\t\t\t?>\n";
			?>
			<ul class="pagination pagination-sm">
			<?php
				echo "\t<?php\n";
				echo "\t\t\t\t\techo \$this->Paginator->prev('&larr; Previous', array('class' => 'prev','tag' => 'li','escape' => false), '<a onclick=\"return false;\">&larr; Previous</a>', array('class' => 'prev disabled','tag' => 'li','escape' => false));\n";
				echo "\t\t\t\t\techo \$this->Paginator->numbers(array('separator' => '','tag' => 'li','currentClass' => 'active','currentTag' => 'a'));\n";
				echo "\t\t\t\t\techo \$this->Paginator->next('Next &rarr;', array('class' => 'next','tag' => 'li','escape' => false), '<a onclick=\"return false;\">Next &rarr;</a>', array('class' => 'next disabled','tag' => 'li','escape' => false));\n";
				echo "\t\t\t\t?>\n";
			?>
			</ul>
			<?php 
				echo "<?php } ?>\n";
			?>
			</div><!-- /.table-responsive -->
			
			
		</div><!-- /.index -->
	
	</div><!-- /#page-content .col-sm-8 -->
	


</div><!-- /#page-container .row-fluid -->

<?php echo "<?php
	echo \$this->Html->script('jquery.min');
	echo \$this->Html->script('plugins/datatables/jquery.dataTables');
	echo \$this->Html->script('plugins/datatables/dataTables.bootstrap');
?>\n"; ?>
<script type="text/javascript">
    $(function() {
        $("#<?php echo str_replace(' ', '', $pluralHumanName); ?>").dataTable();
    });
</script>