<b class="tajuk"><i class="fa fa-bars" aria-hidden="true"></i> IMVC</b> Information Management-Model-View-Controller
<hr>

<div class="row">
  <div class="col-md-4">
<?php echo $this->element('contact'); ?>
  </div>
  <div class="col-md-8">
<div class="box box-primary">
	<div class="box-header with-border">
		<h3 class="box-title"><i class="fa fa-bars" aria-hidden="true"></i> IMVC Framework Checker</h3>
		<div class="box-tools pull-right">
			<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
		</div>
	</div>
	<div class="box-body">
<?php echo $this->element('check'); ?>
	</div>
</div>
  </div>
</div>

