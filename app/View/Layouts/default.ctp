<?php $cakeDescription = __d('cake_dev', 'IMVC'); ?>
<?php echo $this->Html->docType('html5'); ?> 
<html>
	<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<?php echo $this->Html->charset(); ?>

		<title>
			<?php echo $cakeDescription ?>:
			<?php echo $title_for_layout; ?>
		</title>
	<meta name="keywords" content="Keyword"/>
<meta name="description" content="description"/>
<meta name="subject" content="subject">
<meta name="copyright"content="copyright">
<meta name="author" content="author">
<meta name="owner" content="owner">
		<?php 
			echo $this->Html->meta('icon');
			echo $this->Html->meta(['name' => 'viewport', 'content' => 'width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no']);
			echo $this->fetch('meta');
			echo $this->Html->script('jquery-1.11.3.min');
			echo $this->Html->css('bootstrap');
			echo $this->Html->css('custom');
		//	echo $this->Html->css('font-awesome');
			echo $this->Html->css('ionicons.min');
			echo $this->Html->css('AdminLTE');
			echo $this->Html->css('skins/_all-skins');
		//	echo $this->Html->script('ga-local');
			echo $this->fetch('css');
			echo $this->fetch('script');
		?>
	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

	</head>

	<body class="hold-transition skin-blue sidebar-mini">
	
<div class="wrapper">

<!-- Top Menu START -->
<?php echo $this->element('menu/top_menu'); ?>
<!-- Top Menu END -->

<!-- Left Menu START -->
<?php echo $this->element('menu/left_sidebar_public'); ?>

<!-- Left Menu END -->


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Main content -->
    <section class="content">
<aside class="right-side">  

				<section class="content"> 
				<?php echo $this->Session->flash(); ?>
				<?php echo $this->fetch('content'); ?>
				<?php echo $scripts_for_layout; ?>
<?php
if (class_exists('JsHelper') && method_exists($this->Js, 'writeBuffer')) echo $this->Js->writeBuffer();
?>  
				</section>
			</aside><!-- /.right-side -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.0.0
    </div>
    <strong>Copyright &copy; 2018 <a href="https://johor.uitm.edu.my">IMVC</a>.</strong> All rights
    reserved.
  </footer>

<!-- Right Menu START -->
<?php //echo $this->element('menu/right_sidebar'); ?>
<!-- Right Menu END -->
  
</div>
	
	
	


		
		    
			

			
		
		
		<?php
			//echo $this->Html->script('jquery.min');
		?>


		<?php
			echo $this->Html->script('bootstrap.min');
			echo $this->Html->script('jquery-slimscroll/jquery.slimscroll.min');
			echo $this->Html->script('adminlte.min');
			//echo $this->Html->script('CakeAdminLTE/app');
			echo $this->fetch('script');
		?>
		
	</body>

</html>