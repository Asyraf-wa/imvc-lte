<header class="main-header">
    <!-- Logo -->
    <a href="" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b><?php echo $this->Html->image("ims-min.png", array('alt' => 'IMS', 'width'=>'40px')); ?></b><?php //echo $this->Html->image('lynx-200-min.png', array('alt' => 'LYNX', 'width'=>'40px', 'height'=>'40px')); ?></span>
      <!-- logo for regular state and mobile devices 
      <span class="logo-lg"><b><i class="far fa-envelope" aria-hidden="true"></i> -->
	  <?php echo $this->Html->image("logo_small_new.png", array('alt' => 'IMVC', 'width'=>'120px')); ?>
	  </b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
	  <i class="fas fa-bars"></i>
        <span class="sr-only">Toggle navigation</span>
      </a>
	
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
<?php 
	echo $this->Html->image('default_user.jpg', array('alt' => 'No Image', 'width'=>'19px', 'height'=>'19px', 'class' => 'img-circle'));
?>



              <span class="hidden-xs">Administrator</span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                
<?php 
	echo $this->Html->image('default_user.jpg', array('alt' => 'No Image', 'width'=>'90px', 'height'=>'90px', 'class' => 'img-circle'));
?>

                <p>
                  Hello!<br>
				  Administrator
                  <small>Member since June 2018</small>
                </p>
              </li>

              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
	
<?php echo $this->Html->link('<i class="fa fa-user-circle-o"></i> ' .__('Profile'), 
	array('controller'=>'users', 'action'=>'view'),
	array('class'=>'btn btn-default btn-flat', 'title'=>'Sign Out',  'escape' => false)); 
?>

                </div>
                <div class="pull-right">
<?php echo $this->Html->link('<i class="fas fa-sign-out-alt"></i> ' .__('Sign Out'), 
	array('controller' => 'users', 'action' => 'logout'),
	array('class'=>'btn btn-default btn-flat', 'title'=>'Sign Out',  'escape' => false)); 
?>
                </div>
              </li>
            </ul>
          </li>
		  
		  
		  <li class="dropdown user user-menu">

<?php echo $this->Html->link('<i class="fas fa-sign-out-alt" aria-hidden="true"></i> ' .__(''), 
			array('controller' => 'users', 'action' => 'logout'),
			array('escape' => false)); 
		?>

          </li>
		  
		  
        </ul>
      </div>

    </nav>
  </header>