
<!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
		

		<li>
		<?php echo $this->Html->link('<i class="fa fa-home" aria-hidden="true"></i> ' .__('<span> Home</span>'), 
							array('controller' => 'pages', 'action' => 'dashboard'),
							array('escape' => false)); 
			?>
		</li>
  
		<li>
		<?php echo $this->Html->link('<i class="far fa-circle text-lime"></i> ' .__('<span>Coming Soon</span>'), 
							array('controller' => 'pages', 'action' => 'coming_soon'),
							array('escape' => false)); 
			?>
		</li>

      </ul>
	  
	  
    </section>
    <!-- /.sidebar -->
  </aside>